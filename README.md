# Project Ada
An opensource-based research lab experiment computational hub.  Ada, named after [Ada Lovelace](https://en.wikipedia.org/wiki/Ada_Lovelace), was developed to provide a wet-bench and field-use compatible housing enclosure for biological sciences R&D using low cost off-the-shelf components where possible.  Ada was designed as part of multiple collaborations on U.S. Department of Energy sponsored research to build an opensource platform for custom lab instrumentation systems.  

![Printed Ada on a Lab Bench](/Renders and Photos/Pre Release/Ada_v0.9.jpeg "Hello, Ada")*Printed Ada v0.9, Side Panel has an embedded chemical symbol for ATP for those who are curious.*

Ada v1 is designed to use Official Raspberry Pi Touchscreen LCD and Pi-based Single Board Computers.  A printed 150mm DIN-rail is available for mounting industrial and scientific components inside the enclosure, and the side panels can be easily customized for exposing sensor and instrument cabling for whatever experimental setup is required.

## Project Goals

The overall goal of Project Ada is to create an accessible design framework for our biotechnology R&D projects to use in conjunction with common lab sensor instruments, low-cost maker-space components and single board computers, and easily sourced standard parts and connectors.  We needed a wet-bench capable enclosure that could protect our custom instrument boards, provide an interface for logging and visualizing experiment data, and integrate off-the-shelf industrial components.

* Enclosure Should provide splash protection while not compromising user interface (e.g. form follows function)
* Parts should be easily sourced from major suppliers like Digikey, Adafruit, Amazon, etc.
* Minimize part count, standardize screw sizes where possible.
* Ample space for thermal concerns with electronics and ability to expose sensor/communications cables externally.
* Look approachable, but still be a serious lab tool and not ignore the need for field-use.
* Release a workable design for others to customize to their needs as soon as possible (e.g. Perfect is the enemy of good enough)

## Preliminary BOM

ADA is a mostly printed project, with all non-printed parts off the shelf. This is intended to provide a BOM for early adopters, later releases will be packaged with the release files.

0.0.2 BOM

Purchase:
- 1x [Raspberry PI 7" LCD Display](https://www.digikey.com/en/products/detail/raspberry-pi/8997466/6152806)
- 1x [Raspberry PI 4B](https://www.raspberrypi.com/products/raspberry-pi-4-model-b/)
- 1x [USB A to C Panel Mount Cord](https://www.digikey.com/en/products/detail/adafruit-industries-llc/4053/9997695)
- 1x [USB C to C Panel Mount Cord](https://www.digikey.com/en/products/detail/coolgear/CG-U3CMCF20-PM/13561513)

Print:
- 1x Case Frame 0.0.2
- 1x Left Side Plate 0.0.2
- 1x Right Side Plate 0.0.2
- 4x Pin Rear

0.0.1 BOM
- 6x [M3x10 Button Cap Bolts](https://www.mcmaster.com/91290A115/)
- 1x [Raspberry PI 7" LCD Display](https://www.digikey.com/en/products/detail/raspberry-pi/8997466/6152806)
- 1x [Raspberry PI 4B](https://www.raspberrypi.com/products/raspberry-pi-4-model-b/)
- 1x [USB A to C Panel Mount Cord](https://www.digikey.com/en/products/detail/adafruit-industries-llc/4053/9997695)
- 1x [USB C to C Panel Mount Cord](https://www.digikey.com/en/products/detail/coolgear/CG-U3CMCF20-PM/13561513)

BOM NOTES
Mount screws for the USBC panel mounts are included in the parts linked. Mount holes for the ADA case are specifically designed for these parts, swapping with similar connectors is not recommended. 
The Raspberry PI computer mounts directly to the back of the LCD, and all mounting and connection hardware is included with the LCD in the link provided. 

## Preliminary Print Instructions

This project was designed for the Prusa MK3 printer and PrusaSlicer (2.5.0 or above). Ender, Voron, Bambu, and other printers with adequate build space may also work, but you will need to tune settings for your particular machine. [Presliced files FOR PRUSA MK3 MACHINES ONLY are provided here](https://www.printables.com/model/333934-project-ada).

These are large format parts and will require around 2 days of total print time using PLA and recommended settings.

Recommend to print in PLA, but PETG or ABS should also work if you are sufficiently experienced with the printer. Pre-Release ADA units were printed with PolyTerra Cotton White matte PLA and Overture Silk PLA filaments.

Print the Case Frame with the left side on the build plate. In PrusaSlicer, use the overhang painter to put supports on the three bolt seats, but do not put supports inside the bolt hole itself. Use the overhang painter to paint supports on the top of the LCD border.

Print the Side Plates with the large flat faces on the build plate. No supports are required.

Print the Pin Rear components with the head on the build plate. No supports are required. Print with sufficient walls to make the bolts solid. It is recommended to print a few extra bolts, especially if you are opening the case regularly.

Use the 0.2mm Quality print profile. Do not modify any settings on the profile or the support settings (except to enable supports and disable automatic support generation). 


## Preliminary Assembly 

STEP 1: Mount Raspberry Pi to LCD

[LCD to RPi Assembly instructions](https://thepihut.com/blogs/raspberry-pi-tutorials/raspberry-pi-7-touch-screen-assembly-guide).


STEP 2: Install LCD into Frame

Inspect the Case Frame print for imperfections along the LCD slot. The LCD should slide (relatively) smoothly into the slot from the right side of the frame. If there is any debris in the slot this will not succeed and you may break the LCD. Ensure perfect alignment during the installation, tolerances are tight and any misalignment may cause you to break the LCD. Ensure the LCD is facing the correct direction. It should be impossible to install upside down, but if you are experiencing difficulty installing the LCD it may be because the LCD is upside down and the parts are hitting each other. 


STEP 3: Install USB connectors

The front USB port is designed to be a data port. Use the USBA to USBC adapter. Use one of the USB 3.0 ports on the RPi (blue port) if you want to maintain maximum data transfer rates, but fundamentally connecting to any USBA port on the RPi will be functional. Do not overtighted panel bolts, overtightening may cause the case to fracture.

The rear USB port is designed to be a power port. Use the USBC to USBC adapter. Connect the male USBC to the RPi power port, and connect the panel mount to the rear of the frame. Do not overtighted panel bolts, overtightening may cause the case to fracture.


STEP 4: Install Side Plates

Use the Rear Pins to install the side plates to the frame. The pins use a groove system to latch, requiring only a 1/4 turn for full engagement. Do not go beyond a 1/4 turn or you will break the pin. Be careful, the pins are printed components, so they are fragile. Use a standard size flathead screwdriver for installation; an undersized screwdriver may damage the pins. If the pins are not fully seated and the groove doesn't line up, you will break the pin if you try to turn it. Make sure the system is fully seated before turning the pins.


## Pi Software Installation
Follow official Raspberry Pi installation instructions for the [Touch Display](https://www.raspberrypi.com/documentation/accessories/display.html) and the Raspberry Pi [Getting Started Guide](https://projects.raspberrypi.org/en/projects/raspberry-pi-getting-started/).

## Usage
Ada was designed to use off-the-shelf modules from [Widgetlords](https://widgetlords.com/collections/pi-spi-din-series), and other DIN-rail mountable components.  There should be ample space for mounting various GPIO hats, an internal battery, or various Pi-compatible i2c/SPI sensors. 

![Ada Internal Rendering](/Renders and Photos/Pre Release/PI_Case_v19.png "Internal Ada Rendering")*3D Rendering showing Pi LCD inserted, printed DIN-Rail bracket, and rear-facing USB-C mounting point.*

See the associated [LabProbe](https://gitlab.com/LabProbe/labprobe) project for cross-platform Python-based instrumentation libraries to use with your Ada.

## Support
TBD

## Roadmap
A v2 Ada system that can support other single board computers from [Pine64](https://www.pine64.org), [Udoo](https://www.udoo.org), and [nVidia](https://developer.nvidia.com/embedded/jetson-nano-developer-kit) as well as a higher resolution touch display.

A satellite headless enclosure is in the works to match the design language for the current Ada unit.

## Contributing
Please reach out using Gitlab's tools if you'd like to contribute any design modifications to Project Ada.

## Authors and acknowledgment
Designed by [Philip Risser](@phidesign) in collaboration with [Evan Taylor](@evantaylor), [David Baker](@DABAKER165), [Ben Feuer](@penfever), [Brian Ford](@brian-burge) and [Brendan Wacesnke](@thebrendanske) with support from:

[The U.S. Department of Energy SBIR/STTR Office](https://www.energy.gov/science/sbir/small-business-innovation-research-and-small-business-technology-transfer)

[The U.S. Department of Energy Algae Prize](https://www.energy.gov/eere/bioenergy/algaeprize-competition)

[Burge Environmental, Inc.](https://www.burgenv.com)

[The Arizona Center for Algae Technology & Innovation at Arizona State University](https://azcati.asu.edu)

And the numerous researchers, graduate, and undergraduate students at Arizona State University, Colorado State University, National Labs, and others who have been using LabProbe Project tools and code that Ada was designed to work with.

## License
MIT License

## Project status
Actively development until the v2 Ada unit and satellite headless unit designs are finished.  See the [LabProbe](https://gitlab.com/LabProbe/labprobe) and [AzCATI](https://gitlab.com/evantaylor/azcati-cloud) git repos for ongoing python-based tools and and software to use with the Ada platform.
